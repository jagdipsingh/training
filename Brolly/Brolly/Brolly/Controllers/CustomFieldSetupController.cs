﻿namespace Brolly.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Brolly.Models.CustomFields;

    public class CustomFieldSetupController : Controller
    {
        private CustomFieldResolver _customFieldResolver;

        public CustomFieldSetupController(CustomFieldResolver customFieldResolver)
        {
            _customFieldResolver = customFieldResolver;
        }

        public IActionResult Index()
        {
            var model = new CustomFieldSetupViewModel();

            model.CustomFieldTypeComboData = _customFieldResolver.CustomFieldTypesComboData();
            model.CustomFieldEnteredAtComboData = _customFieldResolver.CustomFieldViewTypeComboData();

            return View("CustomFieldSetup", model);
        }
    }
}