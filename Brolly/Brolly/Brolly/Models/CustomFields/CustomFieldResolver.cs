﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace Brolly.Models.CustomFields
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class CustomFieldResolver
    {
        private List<Type> AllCustomFieldTypes()
        {
            var customFieldInterfaceType = typeof(ICustomField);
            return AppDomain.CurrentDomain.GetAssemblies()
                .Select(assembly => assembly.GetTypes()
                    .Where(type => !type.IsInterface && customFieldInterfaceType.IsAssignableFrom(type)))
                .SelectMany(m => m)
                .ToList();
        }

        private List<Type> CustomFieldViewTypes()
        {
            var interfaceType = typeof(ICustomFieldEnteredAt);
            return AppDomain.CurrentDomain.GetAssemblies()
                .Select(assembly => assembly.GetTypes()
                    .Where(type => !type.IsInterface && interfaceType.IsAssignableFrom(type)))
                .SelectMany(m => m)
                .ToList();
        }

        public List<SelectListItem> CustomFieldTypesComboData()
        {
            var types = AllCustomFieldTypes();
            var comboItems = new List<SelectListItem>();
            foreach (var type in types)
            {
                var instance = (ICustomField) Activator.CreateInstance(type);
                comboItems.Add(new SelectListItem(instance.CustomFieldTypeName, instance.CustomFieldType.FullName));
            }

            return comboItems;
        }

        public List<SelectListItem> CustomFieldViewTypeComboData()
        {
            var types = CustomFieldViewTypes();
            var comboItems = new List<SelectListItem>();
            foreach (var type in types)
            {
                var instance = (ICustomFieldEnteredAt)Activator.CreateInstance(type);
                comboItems.Add(new SelectListItem(instance.EntityTypeName, instance.EntityType.FullName));
            }

            return comboItems;
        }


    }
}