﻿using System;

namespace Brolly.Models.CustomFields
{
    public interface ICustomFieldEnteredAt
    {
        Type EntityType { get; }
        string EntityTypeName { get; }
    }
}
