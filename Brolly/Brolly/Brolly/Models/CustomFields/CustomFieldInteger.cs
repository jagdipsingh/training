﻿namespace Brolly.Models.CustomFields
{
    using System;

    public class CustomFieldInteger : ICustomField
    {
        public Type CustomFieldType => typeof(int);
        public int Id { get; }
        public int? CustomFieldOwnerId { get; }
        public string CustomFieldTypeName => "Number";

        public string GetDisplayValue()
        {
            throw new System.NotImplementedException();
        }

        public string GetValue()
        {
            throw new System.NotImplementedException();
        }

        public void SaveValueInstance()
        {
            throw new System.NotImplementedException();
        }

        public bool IsValid()
        {
            throw new System.NotImplementedException();
        }
    }
}