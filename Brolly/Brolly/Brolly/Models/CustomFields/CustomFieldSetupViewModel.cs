﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Brolly.Models.CustomFields
{
    public class CustomFieldSetupViewModel
    {
        public int CustomFieldTypeId { get; set; }
        public int CustomFieldEnteredAtId { get; set; }

        public List<SelectListItem> CustomFieldTypeComboData { get; set; }
        public List<SelectListItem> CustomFieldEnteredAtComboData { get; set; }
    }
}
