﻿namespace Brolly.Models.CustomFields
{
    using System;

    public interface ICustomField
    {
        int Id { get; }
        int? CustomFieldOwnerId { get; }
        string GetDisplayValue();
        string GetValue();
        void SaveValueInstance();
        bool IsValid();
        Type CustomFieldType { get; }
        string CustomFieldTypeName { get; }
    }
}
