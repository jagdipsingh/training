﻿using System;
using System.ComponentModel.DataAnnotations;
using Brolly.Models.CustomFields;

namespace Brolly.Models
{
    public class CharacterViewModel : ICustomFieldEnteredAt
    {
        public int? Id { get; set; }

        [Required]
        [MinLength(3)]
        public string Name { get; set; }

        [Required]
        public int WeightKg { get; set; }

        [Required]
        public string Race { get; set; }

        public Type EntityType => typeof(CharacterViewModel);
        public string EntityTypeName => "Character";
    }
}
