﻿using System.Threading.Tasks;
using Brolly.Data;

namespace Brolly.Models.Character
{
    public class CharacterStore
    {
        public async Task<Data.Character> Add(Data.Character character, DragonBallContext context)
        {
            context.Characters.Add(character);
            await context.SaveChangesAsync();

            return character;
        }
    }
}
