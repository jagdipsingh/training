﻿var characterListComponent = new Vue({
    el: '#character-list-component',
    data: {
        subscribers: [],
        isViewReady: false
    },
    methods: {
        refreshData: function () {
            var self = this;
            this.isViewReady = false;

            $.get("/api/character")
                .done(function (data) {
                    self.characters = data;
                    self.isViewReady = true;
                })
                .fail(function() {
                    alert("error with api call");
                });
        }
    },
    created: function () {
        this.refreshData();
    }
});