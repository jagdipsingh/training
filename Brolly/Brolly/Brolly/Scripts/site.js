﻿new Vue({
    el: '#characterForm',
    data: {
        Name: '',
        Race: '',
        WeightKg: '',
        InvalidName: false,
        IsSubmitted: false
    },
    computed: {
        isSubmitDisabled() {
            var isDisabled = true;

            if (
                this.Name !== '' &&
                this.Race !== '' &&
                this.WeightKg !== ''
            ) {
                isDisabled = false;
            }

            return isDisabled;
        }
    },
    methods: {
        ResetForm() {
            this.Name = '';
            this.Race = '';
            this.WeightKg = '';
            this.InvalidName = false;

            $(this.$refs.SubmitButton).removeClass();
            $(this.$refs.SubmitButton).addClass('btn btn-primary');
        },
        SubmitForm(event) {
            event.preventDefault();
            
            let submit = true;

            if (this.Name.length < 3) {
                submit = false;
                this.InvalidName = true;
            } else {
                this.InvalidName = false;
            }

            if (submit) {
                this.$http.post('/api/character', this.$data)
                    // success
                    .then(response => {
                        // disable submit button
                        $(function () {
                            var $submit = $(this.$refs.SubmitButton);
                            $submit.removeClass('btn-primary');
                            $submit.addClass('btn-success');
                            $submit.switchClass("btn-primary", "btn-success", 800, "easeInOutQuad");

                            this.$refs.SubmitButton.setAttribute("disabled", "disabled");
                            this.$refs.SubmitButton.innerHTML = "Saved";
                        });
                    },
                    // error
                    response => {

                    });
            }
        }
    }
});