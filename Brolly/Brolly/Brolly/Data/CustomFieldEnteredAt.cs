﻿namespace Brolly.Data
{
    public class CustomFieldEnteredAt
    {
        public int Id { get; set; }
        public string EntityType { get; set; }
        public int EntityId { get; set; }
    }
}