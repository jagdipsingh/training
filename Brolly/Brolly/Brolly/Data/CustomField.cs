﻿namespace Brolly.Data
{
    public class CustomField
    {
        public int Id { get; set; }
        public string CustomFieldType { get; set; }
        public int? CustomFieldOwnerId { get; set; }
    }
}
