﻿namespace Brolly.Data
{
    public class CustomFieldValue
    {
        public int Id { get; set; }
        public int CustomFieldId { get; set; }
        public int CustomFieldEnteredAt { get; set; }
        public string Value { get; set; }
        public string DisplayValue { get; set; }
    }
}