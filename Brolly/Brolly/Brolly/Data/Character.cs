﻿namespace Brolly.Data
{
    public class Character
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int WeightKg { get; set; }
        public string Race { get; set; }
    }
}
