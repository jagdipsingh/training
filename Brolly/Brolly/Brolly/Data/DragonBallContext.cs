﻿namespace Brolly.Data
{
    using Microsoft.EntityFrameworkCore;
    using Pomelo.EntityFrameworkCore.MySql.Infrastructure.Internal;

    public class DragonBallContext : DbContext
    {
        public DbSet<Character> Characters { get; set; }
        public DbSet<CustomField> CustomFields { get; set; }
        public DbSet<CustomFieldEnteredAt> CustomFieldEnteredAts { get; set; }
        public DbSet<CustomFieldOwner> CustomFieldOwners { get; set; }
        public DbSet<CustomFieldValue> CustomFieldValues { get; set; }

        private DbContextOptions _options;

        public DragonBallContext(DbContextOptions options)
        {
            _options = options;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL(_options.FindExtension<MySqlOptionsExtension>().ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.Entity<Character>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Name).IsRequired();
            });
        }
    }
}